<?php
//index.php
$connect = mysqli_connect("dallas142", "jtrust08_auction", "abc1234", "jtrust08_auction");
$query = "SELECT * FROM auction_master ORDER BY auctionid ASC";
$result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html>
 <head>
  <title>How to return JSON Data from PHP Script using Ajax Jquery</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  #result {
   position: absolute;
   width: 100%;
   max-width:870px;
   cursor: pointer;
   overflow-y: auto;
   max-height: 400px;
   box-sizing: border-box;
   z-index: 1001;
  }
  .link-class:hover{
   background-color:#f1f1f1;
  }
  </style>
 </head>
 <body>
  <br /><br />
  <div class="container" style="width:900px;">

   <h3 align="center">Search Auctions</h3><br />
   <div class="row">
    <div class="col-md-4">
     <select name="auctionlist" id="auctionlist" class="form-control">
      <option value="">Select city </option>
      <?php
      while($row = mysqli_fetch_array($result))
      {
       echo '<option value="'.$row["auctioncity"].'">'.$row["auctioncity"].'</option>';
      }
      ?>
     </select>
    </div>
    <div class="col-md-4">
     <button type="button" name="search" id="search" class="btn btn-info">Search</button>
    </div>
   </div>
   <br />
   <div class="table-responsive" id="auction_details" style="display:none">
   <table class="table table-bordered">
    <tr>
     <td width="10%" align="right"><b>Auction id</b></td>
     <td width="90%"><span id="auctionid"></span></td>
    </tr>
    <tr>
     <td width="10%" align="right"><b>bankid</b></td>
     <td width="90%"><span id="bankid"></span></td>
    </tr>
   </table>
   </div>

  </div>
 </body>
</html>

<script>
$(document).ready(function(){
 $('#search').click(function(){
  var city= $('#auctionlist').val();
  if(city != '')
  {
   $.ajax({
    url:"fetch.php",
    method:"POST",
    data:{city:city},
    dataType:"JSON",
    success:function(data)
    {
     $('#auction_details').css("display", "block");
     $('#auctionid').text(data.auctionid);
     $('#bankid').text(data.bankid);
    }
   })
  }
  else
  {
   alert("Please Select Employee");
   $('#auction_details').css("display", "none");
  }
 });
});
</script>
