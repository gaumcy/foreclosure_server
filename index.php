<?php include("header.php") ?>
<html>
<head>
  <style>
  .input-group-addon,
input[type=email],
input[type=submit],
.modal-content{border-radius:0px}
</style>
</head>

<body>
  <div class="stats">
  <div class="container">
    <h2>Stats</h2>
    <div class="panel panel-default">
      <div class="panel-body"><?php include ("displaystats.php");?></div>
    </div>
  </div>
  </div>

<div class="latestauctions">
<div class="container">
  <h2>Latest Autions</h2>
  <div class="panel panel-default">
    <div class="panel-body"><?php include ("displaylatestauctions.php");?></div>
  </div>
</div>
</div>

<div class="subscribe">
  <div class="container">
    <h2>Subscribe for Newsletters</h2>
    <div class="panel panel-default">
      <div class="panel-body">
  <?php include('subscription.php');?>
</div>
</div>
</div>
</div>


<br>
<br>

</body>
</html>
<?php include("footer.php") ?>
