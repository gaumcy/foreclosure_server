-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2018 at 11:09 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank_foreclosure`
--

-- --------------------------------------------------------

--
-- Table structure for table `auction_master`
--

CREATE TABLE `auction_master` (
  `auctionid` int(10) NOT NULL,
  `bankid` varchar(10) NOT NULL,
  `auctiontype` varchar(50) NOT NULL,
  `borrowername` varchar(50) NOT NULL,
  `assetcategory` varchar(50) NOT NULL,
  `assettype` varchar(20) NOT NULL,
  `assetdetails` varchar(10000) NOT NULL,
  `assetaddress` varchar(500) NOT NULL,
  `assetlocation` varchar(50) NOT NULL,
  `auctioncity` varchar(50) NOT NULL,
  `auctionprice` int(50) NOT NULL,
  `auctionemd` int(50) NOT NULL,
  `auctionemdmode` varchar(20) NOT NULL,
  `timeextension` int(10) NOT NULL,
  `publicationdt` date NOT NULL,
  `auctionstartdt` date NOT NULL,
  `auctionenddt` date NOT NULL,
  `auctionappdeadline` date NOT NULL,
  `E-auctionprovider` varchar(50) NOT NULL DEFAULT '4-closure',
  `E-auctionurl` varchar(100) NOT NULL DEFAULT 'www.foreclosres.com',
  `auctionapproval` int(3) NOT NULL DEFAULT '0',
  `auctionpriviliged` int(3) DEFAULT '0',
  `auctiondelete` int(3) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auction_master`
--

INSERT INTO `auction_master` (`auctionid`, `bankid`, `auctiontype`, `borrowername`, `assetcategory`, `assettype`, `assetdetails`, `assetaddress`, `assetlocation`, `auctioncity`, `auctionprice`, `auctionemd`, `auctionemdmode`, `timeextension`, `publicationdt`, `auctionstartdt`, `auctionenddt`, `auctionappdeadline`, `E-auctionprovider`, `E-auctionurl`, `auctionapproval`, `auctionpriviliged`, `auctiondelete`) VALUES
(1, 'unionbank', 'Sarfaesi Auction', 'M/s.Kanakadhara Ventures Pvt.Ltd', 'Immovable', 'House', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'havya Krishna Reisdency, Near emerald House, Opp RBI Quarters, Red Hills, Lakdikapool, Hyderabad', 'Lakdikapul', 'Hyderabad', 10000, 4465000, 'DD', 5, '2018-01-27', '2018-02-28', '2018-02-28', '2018-01-26', '4Closure', 'www.bankauctions.in\r\n', 1, 0, 1),
(2, 'unionbank', 'Pisal Auction', 'Mrs.Gautham Ventures Pvt.Ltd', 'Immovable', 'House', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Delhi', 4465000, 4465000, 'DD', 5, '2018-01-15', '2018-01-02', '0000-00-00', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 1, 0, 0),
(3, 'icici', 'Pisal Auction', 'M/r.venkat Ventures Pvt.Ltd', 'Immovable', 'Villa', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Delhi', 4465000, 4465000, 'DD', 5, '2018-02-15', '2018-02-02', '2018-05-03', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 1, 0, 0),
(4, 'icici', 'Pisal Auction', 'M/r.tarun Ventures Pvt.Ltd', 'Immovable', 'Flat', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Goa', 4465000, 4465000, 'DD', 5, '2018-03-15', '2018-02-03', '2018-05-03', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 1, 0, 1),
(5, 'icicigoa', 'Pisal Auction', 'M/r.tarun Ventures Pvt.Ltd', 'Immovable', 'Bike', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Hyderabad', 4465000, 4465000, 'DD', 5, '2018-04-15', '2018-04-02', '2018-05-03', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 1, 0, 1),
(6, 'sbi', 'Pisal Auction', 'M/r.nishanth Ventures Pvt.Ltd', 'Immovable', 'Bike', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Bangalore', 4465000, 4465000, 'DD', 5, '2018-05-15', '2018-05-05', '2018-05-03', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 1, 0, 1),
(7, 'sbi', 'Pisal Auction', 'M/r.nishanth Ventures Pvt.Ltd', 'Immovable', 'Car', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Mumbai', 4465000, 4465000, 'DD', 5, '2018-06-15', '2018-07-06', '2018-00-09', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 1, 0, 1),
(8, 'icici', 'Pisal Auction', 'M/r.venkat Ventures Pvt.Ltd', 'Immovable', 'Villa', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Chennai', 4465000, 4465000, 'DD', 5, '2018-07-15', '2018-02-02', '2018-05-03', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 2, 0, 1),
(9, 'sbi', 'Pisal Auction', 'M/r.Gautham Ventures Pvt.Ltd', 'Immovable', 'Car', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Goa', 4465000, 4465000, 'DD', 5, '2018-08-15', '2018-08-02', '2018-05-03', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 1, 0, 0),
(11, 'unionbhyd', 'Sarfaesi Auction', 'M/s.Kanakadhara Ventures Pvt.Ltd', 'Immovable', 'House', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'havya Krishna Reisdency, Near emerald House, Opp RBI Quarters, Red Hills, Lakdikapool, Hyderabad', 'Lakdikapul', 'Hyderabad', 10000, 4465000, 'DD', 5, '2018-01-27', '2018-02-28', '2018-02-28', '2018-01-26', '4Closure', 'www.bankauctions.in\r\n', 0, 0, 0),
(12, 'unionbmum', 'Pisal Auction', 'M/s.Gautham Ventures Pvt.Ltd', 'Immovable', 'House', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Delhi', 4465000, 4465000, 'DD', 5, '2018-03-15', '2018-02-02', '2018-05-03', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 0, 0, 0),
(13, 'unionbhyd', 'Sarfaesi Auction', 'M/s.Kanakadhara Ventures Pvt.Ltd', 'Immovable', 'House', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'havya Krishna Reisdency, Near emerald House, Opp RBI Quarters, Red Hills, Lakdikapool, Hyderabad', 'Lakdikapul', 'Hyderabad', 10000, 4465000, 'DD', 5, '2018-01-27', '2018-02-28', '2018-02-28', '2018-01-26', '4Closure', 'www.bankauctions.in\r\n', 0, 0, 0),
(14, 'unionbmum', 'Pisal Auction', 'M/s.Gautham Ventures Pvt.Ltd', 'Immovable', 'House', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Delhi', 4465000, 4465000, 'DD', 5, '2018-03-15', '2018-02-02', '2018-05-03', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 0, 0, 0),
(21, 'unionbhyd', 'Sarfaesi Auction', 'M/s.Kanakadhara Ventures Pvt.Ltd', 'Immovable', 'House', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'havya Krishna Reisdency, Near emerald House, Opp RBI Quarters, Red Hills, Lakdikapool, Hyderabad', 'Lakdikapul', 'Hyderabad', 10000, 4465000, 'DD', 5, '2018-01-27', '2018-02-28', '2018-02-28', '2018-01-26', '4Closure', 'www.bankauctions.in\r\n', 0, 0, 0),
(23, 'unionbmum', 'Pisal Auction', 'M/s.Gautham Ventures Pvt.Ltd', 'Immovable', 'House', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Delhi', 4465000, 4465000, 'DD', 5, '2018-03-15', '2018-02-02', '2018-05-03', '2018-03-26', '4Closure', 'www.gautham.in\r\n', 0, 0, 0),
(34, 'icici', 'Pisal Auction', 'M/r.venkat Ventures Pvt.Ltd', 'Immovable', 'Villa', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Chennai', 4465000, 4465000, 'DD', 5, '2018-07-15', '2018-02-02', '2018-05-03', '2018-03-26', '', '', 0, NULL, 0),
(35, 'icici', 'Pisal Auction', 'M/r.venkat Ventures Pvt.Ltd', 'Immovable', 'Villa', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Chennai', 4465000, 4465000, 'DD', 5, '2018-07-15', '2018-02-02', '2018-05-03', '2018-03-26', '', '', 0, NULL, 0),
(36, 'icici', 'Pisal Auction', 'M/r.venkat Ventures Pvt.Ltd', 'Immovable', 'Villa', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Chennai', 4465000, 4465000, 'DD', 5, '2018-07-15', '2018-02-02', '2018-05-03', '2018-03-26', '', '', 0, NULL, 0),
(37, 'icici', 'Pisal Auction', 'M/r.venkat Ventures Pvt.Ltd', 'Immovable', 'Villa', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Chennai', 4465000, 4465000, 'DD', 5, '2018-07-15', '2018-02-02', '2018-05-03', '2018-03-26', '', '', 0, NULL, 0),
(38, 'icici', 'Pisal Auction', 'M/r.venkat Ventures Pvt.Ltd', 'Immovable', 'Villa', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Chennai', 4465000, 4465000, 'DD', 5, '2018-07-15', '2018-02-02', '2018-05-03', '2018-03-26', '', '', 0, NULL, 0),
(39, 'icici', 'Pisal Auction', 'M/r.venkat Ventures Pvt.Ltd', 'Immovable', 'Villa', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Chennai', 4465000, 4465000, 'DD', 5, '2018-07-15', '2018-02-02', '2018-05-03', '2018-03-26', '', '', 0, NULL, 0),
(40, 'iasdcici', 'Piasdsal Auction', '55', 'Immovable', 'Villa', 'House Built up area 1415 sq.ft (including common areas) and car parking area with undivided share of land 45 sq.yards out of 1694 sq.yards', 'Kachiguda Hyderabad', 'PragathiNagar', 'Chennai', 4465000, 4465000, 'DD', 5, '2018-07-15', '2018-02-02', '2018-05-03', '2018-03-26', '', '', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `institution_master`
--

CREATE TABLE `institution_master` (
  `institutionid` varchar(10) NOT NULL,
  `loginid` varchar(50) NOT NULL,
  `institutionname` varchar(50) NOT NULL,
  `institutionmobile` int(15) NOT NULL,
  `institutioncity` varchar(50) NOT NULL,
  `institutionprepname` varchar(50) NOT NULL,
  `institutionprepmobile` int(15) NOT NULL,
  `institutionprepemail` varchar(50) NOT NULL,
  `institutionsrepname` varchar(50) NOT NULL,
  `institutionsrepmobile` int(15) NOT NULL,
  `institutionsrepemail` varchar(50) NOT NULL,
  `institutiondelete` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institution_master`
--

INSERT INTO `institution_master` (`institutionid`, `loginid`, `institutionname`, `institutionmobile`, `institutioncity`, `institutionprepname`, `institutionprepmobile`, `institutionprepemail`, `institutionsrepname`, `institutionsrepmobile`, `institutionsrepemail`, `institutiondelete`) VALUES
('1', 'unionhyd', 'unionbankhyd', 0, 'hyderabad', '', 0, '', '', 0, '', 0),
('2', 'uniondelhi', 'unionbankdelhi', 0, 'delhi', '', 0, '', '', 0, '', 0),
('3', 'icicidelhi', 'icicidelhi', 0, 'delhi', '', 0, '', '', 0, '', 0),
('4', 'icicigoa', 'icicigoa', 0, 'goa', '', 0, '', '', 0, '', 1),
('5', 'sbihyd', 'sbigoa', 0, 'goa', '', 0, '', '', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_master`
--

CREATE TABLE `login_master` (
  `loginid` varchar(20) NOT NULL,
  `loginpassword` varchar(50) NOT NULL,
  `logintype` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_master`
--

INSERT INTO `login_master` (`loginid`, `loginpassword`, `logintype`) VALUES
('admin', 'admin', 'admin'),
('vishu', 'vishu', 'user'),
('bank1', 'bank1', 'bank'),
('icicigoa', 'icicigoa', 'bank'),
('icicidelhi', 'icicidelhi', 'bank'),
('sinz', 'sinz', 'user'),
('sinz1s', 'sinze', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `mailist`
--

CREATE TABLE `mailist` (
  `id` int(20) NOT NULL,
  `mailid` varchar(50) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mailist`
--

INSERT INTO `mailist` (`id`, `mailid`, `status`) VALUES
(1, 'tarun.tenneti@chidhagni.com', 1),
(12, 'gaumcy9@gmail.com', 0),
(13, 'venkat.tangirala@chidhagni.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--

CREATE TABLE `user_master` (
  `username` varchar(50) NOT NULL,
  `loginid` varchar(50) NOT NULL,
  `usermobile` int(15) NOT NULL,
  `usermail` varchar(50) NOT NULL,
  `usercountry` varchar(50) NOT NULL,
  `userstate` varchar(50) NOT NULL,
  `usercity` varchar(50) NOT NULL,
  `useraddress` varchar(50) NOT NULL,
  `userdelete` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_master`
--

INSERT INTO `user_master` (`username`, `loginid`, `usermobile`, `usermail`, `usercountry`, `userstate`, `usercity`, `useraddress`, `userdelete`) VALUES
('vishu', 'vishu', 874569, 'vishu@gmail.com', 'india', 'telangana', 'hyderabda', 'uppal', 1),
('venkat', 'venkat', 45123, 'v@v.com', 'india', 'telangana', 'hyderabd', 'dsnr', 1),
('sinz1', 'sinz1s', 324, 'sinzz1@gmail.com', 'india', 'telangana', 'hyderabad', 'asd', 0),
('gaumcy9', 'gaumcy9', 2147483647, 'gaumcy9@gmail.com', 'india', 'Telangana', 'HYDERABAD', '3-3-146/A,, Chappal bazar,Kachiguda', 0),
('asd', 'asd', 214234, 'asd@gmail.com', 'asd', 'asd', 'asd', 'vsad', 0),
('sinz', 'sinz', 324, 'sinzz@gmail.com', 'india', 'telangana', 'hyderabad', 'asd', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auction_master`
--
ALTER TABLE `auction_master`
  ADD PRIMARY KEY (`auctionid`);

--
-- Indexes for table `institution_master`
--
ALTER TABLE `institution_master`
  ADD PRIMARY KEY (`institutionid`);

--
-- Indexes for table `login_master`
--
ALTER TABLE `login_master`
  ADD PRIMARY KEY (`loginid`);

--
-- Indexes for table `mailist`
--
ALTER TABLE `mailist`
  ADD PRIMARY KEY (`id`,`mailid`);

--
-- Indexes for table `user_master`
--
ALTER TABLE `user_master`
  ADD PRIMARY KEY (`username`,`loginid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auction_master`
--
ALTER TABLE `auction_master`
  MODIFY `auctionid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `mailist`
--
ALTER TABLE `mailist`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
